"""Brikl tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  
from tap_brikl.streams import (
    OrdersStream
)

STREAM_TYPES = [
    OrdersStream
]


class TapBrikl(Tap):
    """Brikl tap class."""
    name = "tap-brikl"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
         th.Property(
            "start_date",
            th.StringType,
            required=True,
            description="The start date to fetch data from"
        ),
        th.Property(
            "shop_id",
            th.StringType,
            required=True,
            description="Shop Id"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
    
if __name__ == "__main__":
    TapBrikl.cli()
