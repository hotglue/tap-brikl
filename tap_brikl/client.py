"""GraphQL client handling, including BriklStream base class."""

import requests
from typing import Any, Dict, Optional, Iterable
from singer_sdk.helpers.jsonpath import extract_jsonpath
from memoization import cached
from singer_sdk.streams import GraphQLStream
from pendulum import parse

class BriklStream(GraphQLStream):
    """Brikl stream class."""

    url_base = "https://api.brikl.com/graphql/admin/public"

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        headers["Authorization"] = f'PAT {self.config.get("access_token")}'
        headers["X-Brikl-Shop-Id"] = f'{self.config.get("shop_id")}'
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def selected_properties(self):
        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                selected_properties.append(field_name)
        return selected_properties


    def gql_selected_fields(self):
        schema = self.schema["properties"]
        catalog = {k: v for k, v in schema.items() if k in self.selected_properties()}

        def denest_schema(schema):
            output = ""
            for key, value in schema.items():
                if "items" in value.keys():
                    value = value["items"]
                if "properties" in value.keys():
                    denested = denest_schema(value["properties"])
                    output = f"{output}\n{key}\n{{{denested}\n}}"
                else:
                    output = f"{output}\n{key}"
            return output

        return denest_schema(catalog)

    def query(self) -> str:
        """Set or return the GraphQL query string."""

        
        base_query = """
            tapBrikl($after: String, $first: Int) {
                __query_name__(after: $after, first: $first){
                    edges {
                        node {
                            __selected_fields__
                        }
                    }
                    pageInfo {
                        hasNextPage
                        startCursor
                        endCursor
                    }
                }
            }
            """
        query = base_query.replace("__query_name__", self.query_name)
        query = query.replace("__selected_fields__", self.gql_selected_fields())

        return query

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Any:
        """Return token identifying next page or None if all records have been read."""
        response_json = response.json()
        has_next_json_path = f"$.data.{self.query_name}.pageInfo.hasNextPage"
        has_next = next(extract_jsonpath(has_next_json_path, response_json), None)

        if has_next:
            endCursor = response_json["data"][self.query_name]["pageInfo"]["endCursor"]
            return endCursor
        return None


    def get_query_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params = {}
        if next_page_token:
            params["after"] = next_page_token
        return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the GraphQL API request."""
        params = self.get_query_params(context, next_page_token)
        query = "query " + self.query()
        request_data = {
            "query": (" ".join([line.strip() for line in query.splitlines()])),
            "variables": params,
        }
        self.logger.debug(f"Attempting query:\n{query}")
        return request_data

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        resp_json = response.json()
        json_path = f"$.data.{self.query_name}.edges[*].node"
        yield from extract_jsonpath(json_path, input=resp_json)
    
    @cached
    def get_starting_time(self, context):
        start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date
    
    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        
        if self.replication_key:
            start_date = self.get_starting_time(context)
            row_start = parse(row.get(self.replication_key))
            if start_date < row_start:
                return row
        else:
            return row