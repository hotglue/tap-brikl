"""Stream type classes for tap-brikl."""

from singer_sdk import typing as th

from tap_brikl.client import BriklStream

class OrdersStream(BriklStream):
    """Define custom stream."""
    name = "orders"
    query_name = "orders"
    replication_key = "createdAt"
    schema = th.PropertiesList(
        th.Property("shippingStatus", th.StringType),
        th.Property("shippingSubTotal", th.NumberType),
        th.Property("shippingTaxTotal", th.NumberType),
        th.Property("shippingTotal", th.NumberType),
        th.Property("paymentStatus", th.StringType),
        th.Property("total", th.NumberType),
        th.Property("type", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("updatedAt", th.DateTimeType),
        th.Property("meta", th.CustomType({"type": ["object", "string"]})),
        th.Property("comment", th.StringType),
        th.Property("notifyToEmail", th.StringType),
        th.Property("organisationId", th.StringType),
        th.Property("cartShippingMethodName", th.StringType),
        th.Property("taxPercentage", th.NumberType),
        th.Property("taxProvider", th.StringType),
        th.Property("taxTotal", th.NumberType),
        th.Property("currencyCode", th.StringType),
        th.Property("externalId", th.StringType),
        th.Property("items", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("sku", th.StringType),
                th.Property("meta", th.CustomType({"type": ["object", "string"]})),
                th.Property("productId", th.StringType),
                th.Property("productVariantId", th.StringType),
                th.Property("imageUrl", th.StringType),
                th.Property("variantTitle", th.StringType),
                th.Property("unitPrice", th.NumberType),
                th.Property("subTotal", th.NumberType),
                th.Property("total", th.NumberType),
                th.Property("title", th.StringType),
                th.Property("taxTotal", th.NumberType),
                th.Property("taxPercentage", th.NumberType),
                th.Property("appliedDiscounts", th.ArrayType(
                    th.ObjectType(
                        th.Property("percent", th.NumberType),
                        th.Property("amount", th.NumberType),
                    )
                )),
                th.Property("quantity", th.NumberType),
            )
        )),
        th.Property("id", th.StringType),
        th.Property("no", th.NumberType),
        th.Property("billingAddress", th.ObjectType(
            th.Property("address1", th.StringType),
            th.Property("address2", th.StringType),
            th.Property("city", th.StringType),
            th.Property("company", th.StringType),
            th.Property("countryCode", th.StringType),
            th.Property("email", th.StringType),
            th.Property("externalId", th.StringType),
            th.Property("externalVendor", th.StringType),
            th.Property("firstName", th.StringType),
            th.Property("id", th.StringType),
            th.Property("isValidAddress", th.StringType),
            th.Property("lastName", th.StringType),
            th.Property("latitude", th.StringType),
            th.Property("longitude", th.StringType),
            th.Property("meta", th.StringType),
            th.Property("organisationId", th.StringType),
            th.Property("phone", th.StringType),
            th.Property("provinceCode", th.StringType),
            th.Property("shopId", th.StringType),
            th.Property("type", th.StringType),
            th.Property("vatNumber", th.StringType),
            th.Property("zip", th.StringType),
        )),
        th.Property("shippingAddressId", th.DateTimeType),
        th.Property("shippingAddress", th.ObjectType(
            th.Property("address1", th.StringType),
            th.Property("address2", th.StringType),
            th.Property("city", th.StringType),
            th.Property("company", th.StringType),
            th.Property("countryCode", th.StringType),
            th.Property("email", th.StringType),
            th.Property("externalId", th.StringType),
            th.Property("externalVendor", th.StringType),
            th.Property("firstName", th.StringType),
            th.Property("id", th.StringType),
            th.Property("isValidAddress", th.StringType),
            th.Property("lastName", th.StringType),
            th.Property("latitude", th.StringType),
            th.Property("longitude", th.StringType),
            th.Property("meta", th.StringType),
            th.Property("organisationId", th.StringType),
            th.Property("phone", th.StringType),
            th.Property("provinceCode", th.StringType),
            th.Property("shopId", th.StringType),
            th.Property("type", th.StringType),
            th.Property("vatNumber", th.StringType),
            th.Property("zip", th.StringType),
        )),
        th.Property("payments", th.ArrayType(
            th.ObjectType(
                th.Property("amount", th.NumberType),
                th.Property("createdAt", th.DateTimeType),
                th.Property("currencyCode", th.StringType),
                th.Property("id", th.StringType),
                th.Property("method", th.StringType),
                th.Property("orderId", th.StringType),
                th.Property("organisationId", th.StringType),
                th.Property("provider", th.StringType),
                th.Property("providerPaymentId", th.StringType),
                th.Property("providerPaymentInfo", th.CustomType({"type": ["object", "string"]})),
                th.Property("providerStatus", th.StringType),
                th.Property("paymentProviderConfigurationId", th.StringType),
                th.Property("shopId", th.StringType),
                th.Property("status", th.StringType),
                th.Property("isTestMode", th.BooleanType),
                th.Property("token", th.StringType),
                th.Property("updatedAt", th.StringType),
            )
        )),
        th.Property("appliedDiscounts", th.ArrayType(
            th.ObjectType(
                th.Property("accountingId", th.StringType),
                th.Property("amount", th.NumberType),
                th.Property("amountBeforeTax", th.NumberType),
                th.Property("taxAmount", th.NumberType),
                th.Property("taxPercentage", th.StringType),
                th.Property("couponCode", th.StringType),
            )
        )),
    ).to_dict()
